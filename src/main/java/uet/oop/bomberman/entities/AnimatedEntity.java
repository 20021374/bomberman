package uet.oop.bomberman.entities;

import javafx.scene.image.Image;
import javafx.scene.shape.Rectangle;
import uet.oop.bomberman.graphics.Sprite;

public abstract class AnimatedEntity extends Entity {
    protected boolean animating;

    protected int frameCounter = 0;
    protected int destinationFrame;

    public AnimatedEntity(Image entityImage, int x, int y) {
        this.entityImage = entityImage;
        this.x = x * Sprite.SCALED_SIZE;
        this.y = y * Sprite.SCALED_SIZE;
        width = entityImage.getWidth();
        height = entityImage.getHeight();
        collider = new Rectangle(this.x, this.y, width, height);
    }

    public void incrementFrame() {
        if (frameCounter < destinationFrame) {
            frameCounter++;
        } else {
            frameCounter = 0;
        }
    }

    public boolean isAnimating() {
        return animating;
    }

    public void setAnimating(boolean flag) {
        animating = flag;
    }
}
