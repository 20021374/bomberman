package uet.oop.bomberman.entities;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.shape.Rectangle;

public abstract class Entity {
    protected double x;
    protected double y;

    protected double width;
    protected double height;

    protected Rectangle collider;

    protected Image entityImage;

    boolean removable = false;

    public Image getImage() {
        return entityImage;
    }

    public void setImage(Image image) {
        entityImage = image;
    }

    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }

    public double getWidth() {
        return this.width;
    }

    public double getHeight() {
        return this.height;
    }

    public abstract void update();
    public abstract void render(GraphicsContext context);

    public void setRemovable(boolean removable) {
        this.removable = removable;
    }

    public boolean isRemovable() {return removable;}

    public Rectangle getCollider() {
        return collider;
    }
}
