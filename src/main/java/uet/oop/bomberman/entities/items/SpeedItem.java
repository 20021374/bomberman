package uet.oop.bomberman.entities.items;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.graphics.Sprite;

public class SpeedItem extends Entity {
    public SpeedItem(Image entityImage, int x, int y) {
        setImage(entityImage);
        this.x = x * Sprite.SCALED_SIZE;
        this.y = y * Sprite.SCALED_SIZE;
    }

    @Override
    public void update() {

    }

    @Override
    public void render(GraphicsContext context) {
        context.drawImage(entityImage, x, y);
    }
}
