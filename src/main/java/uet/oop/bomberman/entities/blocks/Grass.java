package uet.oop.bomberman.entities.blocks;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.graphics.Sprite;

public class Grass extends Entity {

    public Grass(Image image, int x, int y) {
        entityImage = image;
        this.x = x * Sprite.SCALED_SIZE;
        this.y = y * Sprite.SCALED_SIZE;
    }

    @Override
    public void update() {

    }

    @Override
    public void render(GraphicsContext context) {
        context.drawImage(entityImage, x, y);
    }
}
