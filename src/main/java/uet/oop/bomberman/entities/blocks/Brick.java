package uet.oop.bomberman.entities.blocks;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import uet.oop.bomberman.entities.AnimatedEntity;
import uet.oop.bomberman.graphics.Sprite;

public class Brick extends AnimatedEntity {

    private boolean destroyed;

    public Brick(Image entityImage, int x, int y) {
        super(entityImage, x, y);
        destinationFrame = 60;
    }

    @Override
    public void update() {
        if (isAnimating()) {
            incrementFrame();
            updateDeathAnimation();
            if (frameCounter == destinationFrame - 1) {
                destroyed = true;
            }
        }
    }

    @Override
    public void render(GraphicsContext context) {
        context.drawImage(entityImage, x, y);
    }

    private void updateDeathAnimation() {
        entityImage = Sprite.movingSprite(
                Sprite.brick_exploded,
                Sprite.brick_exploded1,
                Sprite.brick_exploded2,
                frameCounter,
                destinationFrame
        ).getFxImage();
    }

    public boolean isDestroyed() {
        return destroyed;
    }
}
