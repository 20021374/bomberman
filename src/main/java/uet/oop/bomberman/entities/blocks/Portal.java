package uet.oop.bomberman.entities.blocks;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.shape.Rectangle;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.graphics.Sprite;

public class Portal extends Entity {
    public Portal(Image entityImage, int x, int y) {
        this.entityImage = entityImage;
        this.x = x * Sprite.SCALED_SIZE;
        this.y = y * Sprite.SCALED_SIZE;
        collider = new Rectangle(x, y, width, height);
    }

    @Override
    public void update() {

    }

    @Override
    public void render(GraphicsContext context) {
        context.drawImage(entityImage, x, y);
    }
}
