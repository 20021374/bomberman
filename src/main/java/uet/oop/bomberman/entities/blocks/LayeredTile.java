package uet.oop.bomberman.entities.blocks;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.shape.Rectangle;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.graphics.Sprite;

import java.util.Collections;
import java.util.LinkedList;

public class LayeredTile extends Entity {
    private LinkedList<Entity> children = new LinkedList<>();

    public LayeredTile(int x, int y, Entity ... entities) {
        this.x = x * Sprite.SCALED_SIZE;
        this.y = y * Sprite.SCALED_SIZE;
        width = 32;
        height = 32;
        Collections.addAll(children, entities);
        collider = new Rectangle(this.x, this.y, width, height);
    }

    @Override
    public void update() {
        for (Entity child : children) {
            child.update();
        }
        clearChildren();
    }

    @Override
    public void render(GraphicsContext context) {
        if (getBrick().isDestroyed()) {
            for (Entity child : children) {
                if (!(child instanceof Brick)) {
                    child.render(context);
                }
            }
        } else {
            getBrick().render(context);
        }
    }

    public Brick getBrick() {
        return (Brick) children.getLast();
    }

    public void destroyBrick() {
        getBrick().setAnimating(true);
    }

    public void clearChildren() {
        children.removeIf(Entity::isRemovable);
    }

    public LinkedList<Entity> getChildren() {
        return children;
    }
}
