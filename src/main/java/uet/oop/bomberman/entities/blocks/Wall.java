package uet.oop.bomberman.entities.blocks;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.shape.Rectangle;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.graphics.Sprite;

public class Wall extends Entity {
    public Wall(Image entityImage, int x, int y) {
        this.entityImage = entityImage;
        this.x = x * Sprite.SCALED_SIZE;
        this.y = y * Sprite.SCALED_SIZE;
        width = entityImage.getWidth();
        height = entityImage.getHeight();
        collider = new Rectangle(this.x, this.y, width, height);
    }

    @Override
    public void update() {

    }

    @Override
    public void render(GraphicsContext context) {
        context.drawImage(entityImage, x, y);
    }
}
