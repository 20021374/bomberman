package uet.oop.bomberman.entities.characters;

import javafx.scene.image.Image;
import uet.oop.bomberman.entities.AnimatedEntity;
import uet.oop.bomberman.floor.Floor;

public abstract class Character extends AnimatedEntity {
    double speed;

    double dx;
    double dy;

    Direction currentDirection = Direction.LEFT;

    private boolean moving;
    private boolean alive;

    protected Floor floor;

    public enum Direction {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }

    public Character(Floor floor, Image entityImage, int x, int y) {
        super(entityImage, x, y);
        alive = true;
        moving = true;
        this.floor = floor;
    }

    public void move() {
        x += dx;
        y += dy;
    }

    public boolean isMoving() {
        return moving;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public void kill() {
        if (alive) {
            alive = false;
        }
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getSpeed() {
        return speed;
    }
}
