package uet.oop.bomberman.entities.characters;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.floor.Floor;
import uet.oop.bomberman.utils.EnumRandomizer;

public class Balloom extends Character {
    private int animationCountDown = 60;

    public Balloom(Floor floor, Image entityImage, int x, int y) {
        super(floor, entityImage, x, y);
        speed = 0.5;
        destinationFrame = 60;
    }

    @Override
    public void update() {
        incrementFrame();
        if (isAlive()) {
            processMovement();
            move();
            randomizeMovement();
            updateSprite();
            collider.setX(x);
            collider.setY(y);
        } else {
            if (isAnimating()) {
                updateDeathAnimation();
            }  else {
                animationCountDown--;
                if (animationCountDown == 0) {
                    setAnimating(true);
                    frameCounter = 0;
                }
            }
        }
    }

    @Override
    public void render(GraphicsContext context) {
        context.drawImage(entityImage, x, y);
    }

    private void processMovement() {
        if (currentDirection == Direction.LEFT) {
            dx = -speed;
        } else if (currentDirection == Direction.RIGHT) {
            dx = speed;
        } else {
            dx = 0;
        }

        if (currentDirection == Direction.UP) {
            dy = -speed;
        } else if (currentDirection == Direction.DOWN) {
            dy = speed;
        } else {
            dy = 0;
        }

        if (x + dx < 0 || x + dx > 960) {
            dx = 0;
        }
        if (y + dy < 0 || y + dy > 384) {
            dy = 0;
        }
    }

    private void updateSprite() {
        if (isMoving()) {
            if (currentDirection == Direction.LEFT || currentDirection == Direction.DOWN) {
                entityImage = Sprite.movingSprite(Sprite.balloom_left1, Sprite.balloom_left2
                        , Sprite.balloom_left3, frameCounter, destinationFrame).getFxImage();
            } else if (currentDirection == Direction.RIGHT || currentDirection == Direction.UP) {
                entityImage = Sprite.movingSprite(Sprite.balloom_right1, Sprite.balloom_right2
                        , Sprite.balloom_right3, frameCounter, destinationFrame).getFxImage();
            }
        }
    }

    private void randomizeMovement() {
        if (frameCounter == destinationFrame - 1) {
            currentDirection = EnumRandomizer.randomEnum(Direction.class);
        }
    }

    private void updateDeathAnimation() {
        destinationFrame = 45;
        entityImage = Sprite.movingSprite(Sprite.mob_dead1, Sprite.mob_dead2
                , Sprite.mob_dead3, frameCounter, destinationFrame).getFxImage();
        if (frameCounter == destinationFrame - 1) {
            setAnimating(false);
            setRemovable(true);
        }
    }

    public void kill() {
        if (isAlive()) {
            setAlive(false);
            setAnimating(true);
            entityImage = Sprite.balloom_dead.getFxImage();
        }
    }

    public void moveBack() {
        dx = 0;
        dy = 0;
    }
}
