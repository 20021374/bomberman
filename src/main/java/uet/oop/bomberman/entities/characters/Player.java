package uet.oop.bomberman.entities.characters;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import uet.oop.bomberman.entities.bombs.Bomb;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.floor.Floor;
import uet.oop.bomberman.managers.InputManager;

import java.util.ArrayList;
import java.util.List;

public class Player extends Character {
    InputManager inputManager = InputManager.getInstance();

    private int bombUp = 1;
    private int blastRadius = 1;

    private List<Bomb> bombStock = new ArrayList<>();

    public Player(Floor floor, Image entityImage, int x, int y) {
        super(floor, entityImage, x, y);
        speed = 1;
        destinationFrame = 15;
        collider.setWidth(width - 6);
        collider.setHeight(height - 6);
    }

    @Override
    public void update() {
        incrementFrame();
        if (isAlive()) {
            processMovementInput();
            if (isMoving()) {
                move();
            }
            updateSprite();
            collider.setX(x);
            collider.setY(y);
        } else {
            if (isAnimating()) {
                updateDeathAnimation();
            }
        }

        updateBomb();

        for (Bomb bomb : bombStock) {
            bomb.update();
        }
    }

    @Override
    public void render(GraphicsContext context) {
        if (isAlive() || isAnimating()) {
            context.drawImage(entityImage, x, y);
        }

        for (Bomb bomb : bombStock) {
            bomb.render(context);
        }
    }

    private void updateSprite() {
        if (isMoving()) {
            if (currentDirection == Direction.LEFT) {
                entityImage = Sprite.movingSprite(Sprite.player_left, Sprite.player_left_1
                        , Sprite.player_left_2, frameCounter, destinationFrame).getFxImage();
            } else if (currentDirection == Direction.RIGHT) {
                entityImage = Sprite.movingSprite(Sprite.player_right, Sprite.player_right_1
                        , Sprite.player_right_2, frameCounter, destinationFrame).getFxImage();
            } else if (currentDirection == Direction.UP) {
                entityImage = Sprite.movingSprite(Sprite.player_up, Sprite.player_up_1
                        , Sprite.player_up_2, frameCounter, destinationFrame).getFxImage();
            } else if (currentDirection == Direction.DOWN) {
                entityImage = Sprite.movingSprite(Sprite.player_down, Sprite.player_down_1
                        , Sprite.player_down_2, frameCounter, destinationFrame).getFxImage();
            }
        }
    }

    private void processMovementInput() {
        if (inputManager.keyDown(KeyCode.UP)) {
            currentDirection = Direction.UP;
            dy = -speed;
        } else if (inputManager.keyDown(KeyCode.DOWN)) {
            currentDirection = Direction.DOWN;
            dy = speed;
        } else {
            dy = 0d;
        }

        if (inputManager.keyDown(KeyCode.LEFT)) {
            currentDirection = Direction.LEFT;
            dx = -speed;
        } else if (inputManager.keyDown(KeyCode.RIGHT)) {
            currentDirection = Direction.RIGHT;
            dx = speed;
        } else {
            dx = 0d;
        }

        setMoving(dx != 0 || dy != 0);
    }

    private void updateBomb() {
        if (inputManager.keyDown(KeyCode.SPACE) && bombStock.size() < bombUp) {
            Bomb bomb = new Bomb(Sprite.bomb.getFxImage(), (int) x, (int) y, this);
            bombStock.add(bomb);
        }

        bombStock.removeIf(bomb -> bomb.isRemovable());
    }

    private void updateDeathAnimation() {
        destinationFrame = 120;
        entityImage = Sprite.movingSprite(Sprite.player_dead1, Sprite.player_dead2
                , Sprite.player_dead3, frameCounter, destinationFrame).getFxImage();
        if (frameCounter == destinationFrame - 1) {
            setAnimating(false);
        }
    }

    public void kill() {
        if (isAlive()) {
            setAlive(false);
            setAnimating(true);
        }
    }

    public void moveBack() {
        setMoving(false);
    }

    public int getBlastRadius() {
        return blastRadius;
    }

    public void setBlastRadius(int blastRadius) {
        this.blastRadius = blastRadius;
    }

    public void setBombUp(int number) {
        bombUp = number;
    }

    public int getBombUp() {
        return bombUp;
    }

    public List<Bomb> getBombStock() {
        return bombStock;
    }
}
