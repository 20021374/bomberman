package uet.oop.bomberman.entities.bombs;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import uet.oop.bomberman.entities.AnimatedEntity;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.entities.characters.Player;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.managers.AudioManager;

import java.util.ArrayList;
import java.util.List;

public class Bomb extends AnimatedEntity {
    private boolean exploded;

    private int explosionCountdown = 120;
    private int explosionDuration = 20;

    private Player owner;
    private List<Flame> flameList = new ArrayList<>();

    AudioManager audioManager = AudioManager.getInstance();

    public Bomb(Image entityImage, int x, int y, Player player) {
        super(entityImage, x / 32, (y + 16 )/ 32);
        destinationFrame = 120;
        owner = player;
    }

    @Override
    public void update() {
        if (explosionCountdown > 0) {
            incrementFrame();
            updateSprite();
            explosionCountdown--;
        } else {
            explode();

            if (explosionDuration > 0) {
                for (Flame flame : flameList) {
                    flame.update();
                }
                flameList.removeIf(Entity::isRemovable);
            } else {
                setRemovable(true);
            }
            explosionDuration--;
        }
    }

    public void render(GraphicsContext context) {
        if (explosionCountdown > 0) {
            context.drawImage(entityImage, x, y);
        } else {
            for (Flame flame : flameList) {
                flame.render(context);
            }
        }
    }

    public void updateSprite() {
        entityImage = Sprite.movingSprite(Sprite.bomb, Sprite.bomb_1
                , Sprite.bomb_2, frameCounter, destinationFrame).getFxImage();
    }

    public void explode() {
        if (!exploded) {
            int intX = (int) x;
            int intY = (int) y;
            int blastRadius = owner.getBlastRadius();
            for (int i = 1; i <= blastRadius; i++) {
                int offset = i * 32;
                Flame flameCenter = new Flame(Sprite.bomb_exploded.getFxImage(), intX, intY, Flame.DIRECTION.CENTER);
                Flame flameLeft = new Flame(Sprite.explosion_horizontal.getFxImage(), intX - offset, intY, Flame.DIRECTION.LEFT);
                Flame flameRight = new Flame(Sprite.explosion_horizontal.getFxImage(), intX + offset, intY, Flame.DIRECTION.RIGHT);
                Flame flameUp = new Flame(Sprite.explosion_vertical.getFxImage(), intX, intY - offset, Flame.DIRECTION.UP);
                Flame flameDown = new Flame(Sprite.explosion_vertical.getFxImage(), intX, intY + offset, Flame.DIRECTION.DOWN);
                if (i == blastRadius) {
                    flameLeft.setTail(true);
                    flameRight.setTail(true);
                    flameUp.setTail(true);
                    flameDown.setTail(true);
                }
                getFlameList().add(flameCenter);
                getFlameList().add(flameLeft);
                getFlameList().add(flameRight);
                getFlameList().add(flameUp);
                getFlameList().add(flameDown);
            }
            exploded = true;
            audioManager.playExplosion();
        }
    }

    public List<Flame> getFlameList() {
        return flameList;
    }

    public void setFlameList(List<Flame> flameList) {
        this.flameList = flameList;
    }
}
