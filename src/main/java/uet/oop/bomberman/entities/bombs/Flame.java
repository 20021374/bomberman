package uet.oop.bomberman.entities.bombs;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import uet.oop.bomberman.entities.AnimatedEntity;
import uet.oop.bomberman.graphics.Sprite;

public class Flame extends AnimatedEntity {
    private boolean tail = false;

    private DIRECTION direction;

    public enum DIRECTION {
        UP,
        RIGHT,
        LEFT,
        DOWN,
        CENTER
    }

    public Flame(Image entityImage, int x, int y, DIRECTION direction) {
        super(entityImage, x / 32, y / 32);
        destinationFrame = 20;
        this.direction = direction;
        collider.setX(x + 1);
        collider.setY(y + 1);
        collider.setWidth(30);
        collider.setHeight(30);
    }

    public void update() {
        incrementFrame();
        updateSprite();
    }

    @Override
    public void render(GraphicsContext context) {
        context.drawImage(entityImage, x, y);
    }

    public void updateSprite() {
        switch (direction) {
            case UP -> {
                if (isTail()) {
                    entityImage = Sprite.movingSprite(Sprite.explosion_vertical_top_last, Sprite.explosion_vertical_top_last1
                            , Sprite.explosion_vertical_top_last2, frameCounter, destinationFrame).getFxImage();
                } else {
                    entityImage = Sprite.movingSprite(Sprite.explosion_vertical, Sprite.explosion_vertical1
                            , Sprite.explosion_vertical2, frameCounter, destinationFrame).getFxImage();
                }
            }
            case DOWN -> {
                if (isTail()) {
                    entityImage = Sprite.movingSprite(Sprite.explosion_vertical_down_last, Sprite.explosion_vertical_down_last1
                            , Sprite.explosion_vertical_down_last2, frameCounter, destinationFrame).getFxImage();
                } else {
                    entityImage = Sprite.movingSprite(Sprite.explosion_vertical, Sprite.explosion_vertical1
                            , Sprite.explosion_vertical2, frameCounter, destinationFrame).getFxImage();
                }
            }
            case LEFT -> {
                if (isTail()) {
                    entityImage = Sprite.movingSprite(Sprite.explosion_horizontal_left_last, Sprite.explosion_horizontal_left_last1
                            , Sprite.explosion_horizontal_left_last2, frameCounter, destinationFrame).getFxImage();
                } else {
                    entityImage = Sprite.movingSprite(Sprite.explosion_horizontal, Sprite.explosion_horizontal1
                            , Sprite.explosion_horizontal2, frameCounter, destinationFrame).getFxImage();
                }
            }
            case RIGHT -> {
                if (isTail()) {
                    entityImage = Sprite.movingSprite(Sprite.explosion_horizontal_right_last, Sprite.explosion_horizontal_right_last1
                            , Sprite.explosion_horizontal_right_last2, frameCounter, destinationFrame).getFxImage();
                } else {
                    entityImage = Sprite.movingSprite(Sprite.explosion_horizontal, Sprite.explosion_horizontal1
                            , Sprite.explosion_horizontal2, frameCounter, destinationFrame).getFxImage();
                }
            }
            case CENTER -> {
                entityImage = Sprite.movingSprite(Sprite.bomb_exploded, Sprite.bomb_exploded1
                        , Sprite.bomb_exploded2, frameCounter, destinationFrame).getFxImage();
            }
        }
    }

    public boolean isTail() {
        return tail;
    }

    public void setTail(boolean tail) {
        this.tail = tail;
    }
}
