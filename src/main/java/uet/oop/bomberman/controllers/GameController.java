package uet.oop.bomberman.controllers;

import javafx.animation.AnimationTimer;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import uet.oop.bomberman.floor.Floor;

import java.net.URL;
import java.util.ResourceBundle;

public class GameController implements Initializable {
    @FXML
    public AnchorPane gameAnchor;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Floor floor = new Floor();
        gameAnchor.getChildren().add(floor);
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long l) {
                floor.render();
                floor.update();
            }
        };
        timer.start();
    }
}

