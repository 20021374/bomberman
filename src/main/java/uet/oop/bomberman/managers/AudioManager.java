package uet.oop.bomberman.managers;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import java.io.File;

public class AudioManager {
    Media media;

    private static AudioManager audioManager = null;

    private AudioManager() {}

    public static AudioManager getInstance() {
        if (audioManager == null) {
            audioManager = new AudioManager();
        }
        return audioManager;
    }

    public void playExplosion() {
        String path = "src/main/resources/uet/oop/bomberman/audios/explosion.wav";
        media = new Media(new File(path).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(media);
        mediaPlayer.play();
    }

    public void playBackGroundMusic() {
        String path = "src/main/resources/uet/oop/bomberman/audios/background.mp3";
        media = new Media(new File(path).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(media);
        mediaPlayer.setAutoPlay(true);
    }
}
