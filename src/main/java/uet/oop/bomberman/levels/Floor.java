package uet.oop.bomberman.levels;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import uet.oop.bomberman.entities.*;
import uet.oop.bomberman.entities.blocks.*;
import uet.oop.bomberman.entities.bombs.Bomb;
import uet.oop.bomberman.entities.bombs.Flame;
import uet.oop.bomberman.entities.characters.Balloom;
import uet.oop.bomberman.entities.characters.Character;
import uet.oop.bomberman.entities.characters.Oneal;
import uet.oop.bomberman.entities.characters.Player;
import uet.oop.bomberman.entities.items.BombItem;
import uet.oop.bomberman.entities.items.FlameItem;
import uet.oop.bomberman.entities.items.SpeedItem;
import uet.oop.bomberman.graphics.Sprite;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Floor extends Canvas {
    public static int WIDTH = Sprite.SCALED_SIZE * 31;
    public static int HEIGHT = Sprite.SCALED_SIZE * 13;

    private boolean levelCleared;
    private boolean gameOver;

    private GraphicsContext context;

    public List<Entity> stillObjects = new ArrayList<>();
    public List<Character> characters = new ArrayList<>();
    protected List<Bomb> bombs = new ArrayList<>();

    public Floor() {
        super(WIDTH, HEIGHT);
        context = getGraphicsContext2D();
        try {
            createMap();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void update() {
        if (levelCleared) {
            drawLevelCleared();
        } else if (gameOver) {
            drawGameOver();
        } else {
            characters.forEach(Entity::update);
            stillObjects.forEach(Entity::update);
            handleCollisions();

            characters.removeIf(Entity::isRemovable);

            for (Character character : characters) {
                if (character instanceof Player) {
                    if (!character.isAlive() && !character.isAnimating()) {
                        gameOver = true;
                    }
                }
            }
        }
    }

    public void render() {
        if (!gameOver && !levelCleared) {
            context.clearRect(0, 0, getWidth(), getHeight());
            stillObjects.forEach(g -> g.render(context));
            characters.forEach(g -> g.render(context));
        }
    }

    public void createMap() throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("src/main/resources/uet/oop/bomberman/levels/Level1.txt"));
        int currentLevel = scanner.nextInt();
        HEIGHT = scanner.nextInt();
        WIDTH = scanner.nextInt();
        scanner.nextLine();

        for (int i = 0; i < HEIGHT; i++) {
            String parser = scanner.nextLine();
            for (int j = 0; j < WIDTH; j++) {
                switch (parser.charAt(j)) {
                    case ' ' -> {
                        stillObjects.add(new Grass(Sprite.grass.getFxImage(), j, i));
                    }
                    case '#' -> {
                        stillObjects.add(new Wall(Sprite.wall.getFxImage(), j, i));
                    }
                    case 'x' -> {
                        stillObjects.add(new LayeredTile(j, i,
                                new Grass(Sprite.grass.getFxImage(), j, i),
                                new Portal(Sprite.portal.getFxImage(), j, i),
                                new Brick(Sprite.brick.getFxImage(), j, i)));
                    }
                    case '*' -> {
                        stillObjects.add(new LayeredTile(j, i,
                                new Grass(Sprite.grass.getFxImage(), j, i),
                                new Brick(Sprite.brick.getFxImage(), j, i)));
                    }
                    case 'p' -> {
                        characters.add(new Player(this, Sprite.player_down.getFxImage(), j, i));
                        stillObjects.add(new Grass(Sprite.grass.getFxImage(), j, i));
                    }
                    case '2' -> {
                        characters.add(new Oneal(this, Sprite.oneal_left1.getFxImage(), j, i));
                        stillObjects.add(new Grass(Sprite.grass.getFxImage(), j, i));
                    }
                    case '1' -> {
                        characters.add(new Balloom(this, Sprite.balloom_left1.getFxImage(), j, i));
                        stillObjects.add(new Grass(Sprite.grass.getFxImage(), j, i));
                    }
                    case 'b' -> {
                        stillObjects.add(new LayeredTile(j, i,
                                new Grass(Sprite.grass.getFxImage(), j, i),
                                new BombItem(Sprite.powerup_bombs.getFxImage(), j, i),
                                new Brick(Sprite.brick.getFxImage(), j, i)));
                    }
                    case 's' -> {
                        stillObjects.add(new LayeredTile(j, i,
                                new Grass(Sprite.grass.getFxImage(), j, i),
                                new SpeedItem(Sprite.powerup_speed.getFxImage(), j, i),
                                new Brick(Sprite.brick.getFxImage(), j, i)));
                    }
                    case 'f' -> {
                        stillObjects.add(new LayeredTile(j, i,
                                new Grass(Sprite.grass.getFxImage(), j, i),
                                new FlameItem(Sprite.powerup_flames.getFxImage(), j, i),
                                new Brick(Sprite.brick.getFxImage(), j, i)));
                    }
                }
            }
        }
    }

    public void handleCollisions() {
        Player player = null;
        for (Character character : characters) {
            for (Character character1 : characters) {
                if (character instanceof Player) {
                    player = (Player) character;
                    if (!(character1 instanceof Player)) {
                        if (character1.isAlive()) {
                            if (player.getCollider().getBoundsInParent()
                                    .intersects(character1.getCollider().getBoundsInParent())) {
                                player.kill();
                            }
                        }
                    }
                }
            }
        }
        if (player != null) {
            bombs = player.getBombStock();
            for (Bomb bomb : bombs) {
                for (Flame flame : bomb.getFlameList()) {
                    for (Entity entity : stillObjects) {
                        if (entity instanceof LayeredTile tile) {
                            if (flame.getCollider().getBoundsInParent()
                                    .intersects(tile.getCollider().getBoundsInParent())) {
                                tile.destroyBrick();
                            }
                        }
                    }
                    for (Character character : characters) {
                        if (flame.getCollider().getBoundsInParent()
                                .intersects(character.getCollider().getBoundsInParent())) {
                            character.kill();
                        }
                    }
                }
            }


            for (Entity entity : stillObjects) {
                if (entity instanceof LayeredTile tile) {
                    if (tile.getBrick().isDestroyed()) {
                        if (player.getCollider().getBoundsInParent()
                                .intersects(tile.getCollider().getBoundsInParent())) {
                            for (Entity child : tile.getChildren()) {
                                if (child instanceof SpeedItem) {
                                    player.setSpeed(player.getSpeed() + 0.5);
                                    child.setRemovable(true);
                                } else if (child instanceof FlameItem) {
                                    player.setBlastRadius(player.getBlastRadius() + 1);
                                    child.setRemovable(true);
                                } else if (child instanceof BombItem) {
                                    player.setBombUp(player.getBombUp() + 1);
                                    child.setRemovable(true);
                                } else if (child instanceof Portal) {
                                    if (stageCleared()) {
                                        levelCleared = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public boolean stageCleared() {
        return characters.size() == 1;
    }

    public void drawLevelCleared() {
        context.clearRect(0, 0, getWidth(), getHeight());
        context.setFill(Color.web("#222222"));
        context.fillRect(0, 0, getWidth(), getHeight());
        context.setFill(Color.WHITE);
        context.setFont(Font.font("Arial", 30));
        context.fillText("Level Cleared", getWidth() / 2 - 50, getHeight() / 2);
    }

    public void drawGameOver() {
        context.clearRect(0, 0, getWidth(), getHeight());
        context.setFill(Color.web("#222222"));
        context.fillRect(0, 0, getWidth(), getHeight());
        context.setFill(Color.WHITE);
        context.setFont(Font.font("Arial", 30));
        context.fillText("GAME OVER", getWidth() / 2 - 50, getHeight() / 2);
    }
}
