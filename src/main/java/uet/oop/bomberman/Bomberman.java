package uet.oop.bomberman;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import uet.oop.bomberman.managers.AudioManager;
import uet.oop.bomberman.managers.InputManager;

import java.io.IOException;


public class Bomberman extends Application {

    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/uet/oop/bomberman/fxml/MainView.fxml"));
        Scene scene = new Scene(root);
        InputManager.getInstance().pollScene(scene);
        primaryStage.setTitle("Bomberman");
        primaryStage.setScene(scene);
        primaryStage.show();
        AudioManager.getInstance().playBackGroundMusic();
    }

    public static void main(String[] args) {
        launch();
    }
}