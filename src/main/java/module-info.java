module uet.oop.bomberman {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;
    requires javafx.media;

    opens uet.oop.bomberman.controllers to javafx.fxml;
    exports uet.oop.bomberman;
    opens uet.oop.bomberman.managers to javafx.fxml;
    exports uet.oop.bomberman.floor;
}